# Comandi utili per git

## Contenuti

1. [Comandi base](#comandi-base)
2. [Comandi base git](#comandi-base-git)
3. [Comandi con checkout](#comandi-con-checkout)
4. [Comandi per GitLab](#comandi-per-gitlab)

---

## Comandi base

mkdir nome = crea una repositery

cd = permette di muversi tra i file

touch = crea un file

nano = permette di scrivere in un file

clear = permette di pulire la mia schermata

---

## Comandi base git

git config user.name "nome" = imposti il nome utente

git config user.mail "mail" = imposti la mail dell utente

git init = ti crea una cartella init in una repository che contiene tutti i file principali.

git status = permette di vedere lo stato delle mie modifiche

git add nome = permette di aggiunger un file in locale

git commit = mi da la posssibilità di salvare le modifiche in locale aggiungendo un messaggio, se la modifica che ho fato è piccola posso aggiungere -m alla fine e scrivere direttamente li il messaggio

git log = permette di vedere tutti i commit effetuati nel tempo

---

## Comandi con cehckout

git checkout idCommit = permette di tornare indietro nel tempo, cioe ti riporta al momento in cui hai effettuato quel commit

git checkout -b nome = permette di creare un nuovo branch

git branch = permette di vedere tutti i branch creati

git checkout master = mi permette di tornare al branch principale

git merge = permette di unire tra di loro due branch o solitamente un branch e il master e collega le due storie

---

## Comandi per GitLab

git remote add origin git@gitlab.com:username/nomeProgetto.git = permette di collegarmi da remoto con un progetto su GitLab

git push origin nome = permette di inviare le modifiche online da remoto

git pull origin nome = permette di ricevere file da remoto
